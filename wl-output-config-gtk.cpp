/*
 * wayland-scanner client-header /usr/share/wayland-protocols/unstable/xdg-output/xdg-output-unstable-v1.xml xdg-output-unstable-v1-client-protocol.h
 * wayland-scanner private-code /usr/share/wayland-protocols/unstable/xdg-output/xdg-output-unstable-v1.xml xdg-output-unstable-v1-client-protocol.c
 * 
 * gcc -c xdg-output-unstable-v1-client-protocol.c -o xdg-output.o
 * g++ $(pkg-config --cflags gtk+-3.0) gtk-wl-output.cpp xdg-output.o $(pkg-config --libs gtk+-3.0 wayland-client)
 */

#include <gdk/gdkwayland.h>
#include <gtk/gtk.h>
#include <cairo.h>
#include <vector>
#include <string>
#include <cstring>
#include <iomanip>
#include <sstream>
#include <wayland-client.h>
#include "xdg-output-unstable-v1-client-protocol.h"

using namespace std;

class Rect
{
        public:
        int32_t x, y, width, height;

        Rect(int32_t x, int32_t y, int32_t width, int32_t height) {
                this->x = x;
                this->y = y;
                this->width = width;
                this->height = height;
        };
};

class Output;

class OutputRect
{
        public:
        Output *output;
        Rect *rect;

        OutputRect(Output *output, Rect *rect) {
                this->output = output;
                this->rect = rect;
        };
};

class OutputConfig
{
        public:
        GtkWidget *outputs_cb;
        GtkWidget *make_data;
        GtkWidget *model_data;
        GtkWidget *pos_data;
        GtkWidget *size_data;
        GtkWidget *res_cb;
        GtkWidget *rot_cb;
};

class OutputMode
{
        public:
        uint32_t flags;
        int32_t width, height;
        int32_t refresh;
        uint8_t cb_index;
};

class Output
{
        public:
        struct wl_output *wl_output;
        int32_t x, y;
        int32_t physical_width, physical_height;
        int32_t subpixel;
        const char *make, *model, *config_name;
        int32_t transform;
        int32_t scale;
        vector<OutputMode *> modes;
        OutputRect *rect;
        uint8_t selected;
        uint8_t cb_index;
};

class Window
{
        public:
        struct zxdg_output_manager_v1 *output_manager;
        cairo_surface_t *surface;
        vector<Output *> outputs;
        OutputConfig config;
};

static void
output_handle_geometry(void *data, struct wl_output *wl_output,
                       int32_t x, int32_t y,
                       int32_t physical_width, int32_t physical_height,
                       int32_t subpixel,
                       const char *make, const char *model,
                       int32_t transform)
{
        Output *output = (Output *) data;

        output->wl_output = wl_output;
        output->x = x;
        output->y = y;
        output->physical_width = physical_width;
        output->physical_height = physical_height;
        output->subpixel = subpixel;
        output->make = strdup(make);
        output->model = strdup(model);
        output->transform = transform;
}

static void
output_handle_mode(void *data, struct wl_output *wl_output,
                   uint32_t flags, int32_t width, int32_t height,
                   int32_t refresh)
{
        Output *output = (Output *) data;

        OutputMode *mode = new OutputMode();

        mode->flags = flags;
        mode->width = width;
        mode->height = height;
        mode->refresh = refresh;

        output->modes.push_back(mode);
}

static void
output_handle_done(void *data, struct wl_output *wl_output)
{
}

static void
output_handle_scale(void *data, struct wl_output *wl_output,
                    int32_t scale)
{
}

static const struct wl_output_listener output_listener = {
        output_handle_geometry,
        output_handle_mode,
        output_handle_done,
        output_handle_scale,
};

static void
zxdg_output_handle_logical_position(void *data,
                                    struct zxdg_output_v1 *zxdg_output_v1,
                                    int32_t x,
                                    int32_t y)
{
}

static void
zxdg_output_handle_logical_size(void *data,
                                struct zxdg_output_v1 *zxdg_output_v1,
                                int32_t width,
                                int32_t height)
{
}

static void
zxdg_output_handle_done(void *data,
                        struct zxdg_output_v1 *zxdg_output_v1)
{
}

static void
zxdg_output_handle_name(void *data,
                        struct zxdg_output_v1 *zxdg_output_v1,
                        const char *name)
{
        Output *output = (Output *) data;

        output->config_name = strdup(name);
}

static void
zxdg_output_handle_description(void *data,
                               struct zxdg_output_v1 *zxdg_output_v1,
                               const char *description)
{
}

static const struct zxdg_output_v1_listener zxdg_output_listener = {
        zxdg_output_handle_logical_position,
        zxdg_output_handle_logical_size,
        zxdg_output_handle_done,
        zxdg_output_handle_name,
        zxdg_output_handle_description,
};

void registry_add_object(void *data, struct wl_registry *registry, uint32_t id,
                         const char *interface, uint32_t version)
{
        Window *window = (Window *) data;
        struct zxdg_output_v1 *zxdg_output;
        struct wl_output *wl_output;
        Output *output;

        if (strcmp(interface, zxdg_output_manager_v1_interface.name) == 0) {
                window->output_manager = (struct zxdg_output_manager_v1*)
                wl_registry_bind(registry, id, &zxdg_output_manager_v1_interface, version);
        } else if (strcmp(interface, wl_output_interface.name) == 0) {
                output = new Output();
                wl_output = (struct wl_output*)
                        wl_registry_bind(registry, id, &wl_output_interface, version);
                wl_output_add_listener(wl_output, &output_listener, output);
                if (window->output_manager) {
                        zxdg_output = zxdg_output_manager_v1_get_xdg_output(window->output_manager, wl_output);
                        zxdg_output_v1_add_listener(zxdg_output, &zxdg_output_listener, output);
                }
                window->outputs.push_back(output);
        }
}

void registry_remove_object(void *data, struct wl_registry *registry,
                            uint32_t id)
{
}

static struct wl_registry_listener registry_listener =
{
        &registry_add_object,
        &registry_remove_object
};

static void
fill_rect(cairo_t *cr, double r, double g, double b, double a,
          double x, double y, double w, double h)
{
        cairo_set_source_rgba(cr, r, g, b, a);
        cairo_rectangle(cr, x, y, w, h);
        cairo_fill(cr);
}

static void
draw_rect(cairo_t *cr, double r, double g, double b,
          double x, double y, double w, double h)
{
        cairo_set_source_rgb(cr, r, g, b);
        cairo_rectangle(cr, x, y, w, h);
        cairo_stroke(cr);
}

static char *
get_formatted_string(cairo_t *cr, const char *s, double *x, double w,
                     cairo_text_extents_t *extents)
{
        char *str;
        int len, center = 1;

        str = strdup(s);

        while (1) {
                cairo_text_extents(cr, str, extents);
                if (extents->width <= w) {
                        *x += 10;
                        if (center)
                                *x += w * 0.5 - extents->width * 0.5;
                        return str;
                } else {
                        center = 0;
                        len = strlen(str);
                        if (len < 5)
                                return str;
                        strcpy(&str[len - 4], "...");
                }
        }
}

static void
draw_text(cairo_t *cr, double r, double g, double b,
          double x, double y, double w, double h,
          double size, const char *s)
{
        cairo_text_extents_t extents;
        char *str;

        cairo_select_font_face(cr, "Arial",
        CAIRO_FONT_SLANT_NORMAL,
        CAIRO_FONT_WEIGHT_BOLD);

        cairo_set_font_size(cr, size);

        str = get_formatted_string(cr, s, &x, w - 20, &extents);

        cairo_move_to(cr, x, y + h * 0.5 + extents.height * 0.5);
        cairo_set_source_rgb(cr, r, g, b);

        cairo_show_text(cr, str);

        free(str);
}

static void
draw_output(Output *output, cairo_t *cr, double x, double y, double w, double h)
{
        fill_rect(cr, 0.34, 0.70, 0.87, output->selected ? 1.0 : 0.59, x, y, w, h);
        draw_rect(cr, 0.30, 0.45, 0.67, x, y, w, h);
        draw_text(cr, 1.0, 1.0, 1.0, x, y, w, h,
                        14, (to_string(output->cb_index) + ": " +
                        string(output->make) + ": " +
                        string(output->model)).c_str());
        delete output->rect;
        output->rect = new OutputRect(output, new Rect(x, y, w, h));
}

static void
draw_outputs(GtkWidget *widget,
             cairo_t *cr,
             gpointer data)
{
        Window *window = (Window *) data;
        int x1, y1, x2, y2;
        int w = gtk_widget_get_allocated_width(widget);
        int h = gtk_widget_get_allocated_height(widget);
        int32_t width, height;
        int i, j, x, y;
        int diff_width, diff_height;
        double scale_factor, middle;
        vector<OutputRect> output_rects;

        x1 = y1 = x2 = y2 = x = y = width = height = 0;

        for (i = 0; i < int(window->outputs.size()); i++) {
                Output *output = window->outputs[i];
                x = output->x;
                y = output->y;
                for (j = 0; j < int(output->modes.size()); j++) {
                        OutputMode *mode = output->modes[j];
                        if (mode->flags) {
                                width = mode->width;
                                height = mode->height;
                        }
                }
                x1 = x1 < x ? x1 : x;
                y1 = y1 < y ? y1 : y;
                x2 = x2 > x + width ? x2 : x + width;
                y2 = y2 > y + height ? y2 : y + height;
                OutputRect output_rect =
                        OutputRect(output, new Rect(x, y, width, height));
                output_rects.push_back(output_rect);
        }

        width = x2 - x1;
        height = y2 - y1;
        diff_width = width - (w - 20);
        diff_height = height - (h - 20);

        for (i = 0; i < int(output_rects.size()); i++) {
                OutputRect rect = output_rects[i];
                scale_factor = (w - 20) / (double) width;
                middle = (h - 20) * 0.5 + 10;
                if (diff_width > diff_height && (rect.rect->y * scale_factor + (middle - height * scale_factor * 0.5)) > 10) {
                        x = rect.rect->x * scale_factor + 10;
                        y = rect.rect->y * scale_factor + (middle - height * scale_factor * 0.5);
                } else {
                        scale_factor = (h - 20) / (double) height;
                        middle = (w - 20) * 0.5 + 10;
                        x = rect.rect->x * scale_factor + (middle - width * scale_factor * 0.5);
                        y = rect.rect->y * scale_factor + 10;
                }
                draw_output(rect.output, cr, x, y,
                            rect.rect->width * scale_factor,
                            rect.rect->height * scale_factor);
        }
}

static gboolean
draw(GtkWidget *widget,
     cairo_t *cr,
     gpointer data)
{
        int w = gtk_widget_get_allocated_width(widget);
        int h = gtk_widget_get_allocated_height(widget);

        cairo_set_source_rgb(cr, 1, 1, 1);
        cairo_paint(cr);

        draw_rect(cr, 0.5, 0.5, 0.5, 0, 0, w, h);

        draw_outputs(widget, cr, data);

        gtk_widget_queue_draw_area(widget, 0, 0, w, h);

        return FALSE;
}

static gboolean
draw_area_button_press_cb(GtkWidget *widget,
                          GdkEventButton *event,
                          gpointer data)
{
        Window *window = (Window *) data;
        Output *output;
        int i;

        for (i = 0; i < int(window->outputs.size()); i++) {
                output = window->outputs[i];
                if (event->x > output->rect->rect->x && event->y > output->rect->rect->y &&
                    event->x < output->rect->rect->x + output->rect->rect->width &&
                    event->y < output->rect->rect->y + output->rect->rect->height) {
                        gtk_combo_box_set_active(GTK_COMBO_BOX(window->config.outputs_cb), output->cb_index);
                }
        }
}

static Output *
lookup_output_by_index(Window *window, int index)
{
        Output *output = NULL;
        int i;

        for (i = 0; i < int(window->outputs.size()); i++) {
                if (window->outputs[i]->cb_index == index) {
                        output = window->outputs[i];
                        window->outputs[i]->selected = 1;
                } else {
                        window->outputs[i]->selected = 0;
                }
        }

        return output;
}

static Output *
add_outputs(Window *window, GtkWidget *widget)
{
        int i;
        Output *output = NULL;
        char name[256];

        for (i = 0; i < int(window->outputs.size()); i++) {
                gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(widget),
                        (string(window->outputs[i]->make) + ": " +
                        string(window->outputs[i]->model)).c_str());
                window->outputs[i]->cb_index = i;
                if (i == 0) {
                        output = window->outputs[i];
                        output->selected = 1;
                }
        }

        return output;
}

static void
add_modes(Output *output, GtkWidget *widget)
{
        int i;

        gtk_combo_box_text_remove_all(GTK_COMBO_BOX_TEXT(widget));

        for (i = 0; i < output->modes.size(); i++) {
                OutputMode *mode = output->modes[i];
                double refresh = mode->refresh * 0.001;
                stringstream stream;
                stream << fixed << setprecision(2) << refresh;
                string refresh_str = stream.str();
                gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(widget),
                                        (to_string(mode->width) + "x" +
                                        to_string(mode->height) + "@" +
                                        refresh_str).c_str());
                mode->cb_index = i;
                if (mode->flags)
                        gtk_combo_box_set_active(GTK_COMBO_BOX(widget), i);
        }
}

static GtkWidget *
add_label(const char *tag, const char *info, GtkWidget **ret_data)
{
        GtkWidget *layout = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
        GtkWidget *label = gtk_label_new(tag);
        GtkWidget *data = gtk_label_new(info);
        gtk_box_pack_start(GTK_BOX(layout), label, false, false, 0);
        gtk_box_pack_end(GTK_BOX(layout), data, false, false, 0);

        *ret_data = data;
        return layout;
}

static void
output_changed_cb(GtkComboBox *widget,
                  gpointer user_data)
{
        Window *window = (Window *) user_data;
        Output *output;

        output = lookup_output_by_index(window, gtk_combo_box_get_active(GTK_COMBO_BOX(widget)));

        if (!output)
                return;

        gtk_label_set_text(GTK_LABEL(window->config.make_data), output->make);
        gtk_label_set_text(GTK_LABEL(window->config.model_data), output->model);
        gtk_label_set_text(GTK_LABEL(window->config.pos_data),
                           (to_string(output->x) + ", " +
                           to_string(output->y)).c_str());
        gtk_label_set_text(GTK_LABEL(window->config.size_data),
                           (to_string(output->physical_width) + " x " +
                           to_string(output->physical_height)).c_str());
        add_modes(output, window->config.res_cb);
        gtk_combo_box_set_active(GTK_COMBO_BOX(window->config.rot_cb), output->transform);
}

static gboolean
close_button_release_cb(GtkWidget *widget,
                       GdkEventButton *event,
                       gpointer data)
{
        int x = event->x;
        int y = event->y;
        int w = gtk_widget_get_allocated_width(widget);
        int h = gtk_widget_get_allocated_height(widget);

        if (x > 0 && y > 0 && x < w && y < h)
                exit(0);
}

static gboolean
apply_button_press_cb(GtkWidget *widget,
                      GdkEventButton *event,
                      gpointer data)
{
        printf("%s\n", __func__);
}

static void
activate(GtkApplication* app,
         gpointer user_data)
{
        Window *win = (Window *) user_data;
        struct wl_registry *registry;
        struct wl_display *display = gdk_wayland_display_get_wl_display(gdk_display_get_default());
        GtkWidget *window;

        registry = wl_display_get_registry(display);

        wl_registry_add_listener(registry, &registry_listener, user_data);
        wl_display_dispatch(display);
        wl_display_roundtrip(display);
        wl_registry_destroy(registry);


        window = gtk_application_window_new(app);
        gtk_widget_set_size_request(window, 640, 480);
        gtk_window_set_default_size(GTK_WINDOW(window), 830, 530);

        GtkWidget *main_layout = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);

        GtkWidget *main_label = gtk_label_new(NULL);
        gtk_label_set_markup(GTK_LABEL(main_label), "<b>Displays</b>");

        GtkWidget *interface_layout = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);

        GtkWidget *drawing_area = gtk_drawing_area_new();

        GtkWidget *config_layout = gtk_box_new(GTK_ORIENTATION_VERTICAL, 10);
        gtk_widget_set_size_request(config_layout, 300, 10);

        GtkWidget *outputs_cb = gtk_combo_box_text_new();
        Output *default_output = add_outputs(win, outputs_cb);
        gtk_combo_box_set_active(GTK_COMBO_BOX(outputs_cb), 0);

        GtkWidget *enabled_layout = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
        GtkWidget *enabled_cb = gtk_check_button_new();
        GtkWidget *enabled_label = gtk_label_new("Enabled");
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(enabled_cb), 1);
        gtk_box_pack_start(GTK_BOX(enabled_layout), enabled_cb, false, false, 0);
        gtk_box_pack_end(GTK_BOX(enabled_layout), enabled_label, false, false, 0);

        GtkWidget *make_data, *model_data, *pos_data, *size_data;
        GtkWidget *make_layout = add_label("Make:", default_output->make, &make_data);
        GtkWidget *model_layout = add_label("Model:", default_output->model, &model_data);
        GtkWidget *pos_layout =
                add_label("Position:", (to_string(default_output->x) +
                        ", " + to_string(default_output->y)).c_str(),
                        &pos_data);
        GtkWidget *size_layout =
                add_label("Physical Size:", (to_string(default_output->physical_width) +
                           " x " + to_string(default_output->physical_height)).c_str(),
                           &size_data);

        GtkWidget *res_layout = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
        GtkWidget *res_label = gtk_label_new("Resolution:");
        GtkWidget *res_cb = gtk_combo_box_text_new();
        add_modes(default_output, res_cb);
        gtk_box_pack_start(GTK_BOX(res_layout), res_label, false, false, 0);
        gtk_box_pack_end(GTK_BOX(res_layout), res_cb, false, false, 0);

        GtkWidget *rot_layout = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
        GtkWidget *rot_label = gtk_label_new("Rotation:");
        GtkWidget *rot_cb = gtk_combo_box_text_new();
        gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(rot_cb), "Normal");
        gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(rot_cb), "Right");
        gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(rot_cb), "Inverted");
        gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(rot_cb), "Left");
        gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(rot_cb), "Flipped");
        gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(rot_cb), "Right Flipped");
        gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(rot_cb), "Inverted Flipped");
        gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(rot_cb), "Left Flipped");
        gtk_combo_box_set_active(GTK_COMBO_BOX(rot_cb), 0);
        gtk_box_pack_start(GTK_BOX(rot_layout), rot_label, false, false, 0);
        gtk_box_pack_end(GTK_BOX(rot_layout), rot_cb, false, false, 0);

        gtk_box_pack_start(GTK_BOX(config_layout), outputs_cb, false, true, 0);
        gtk_box_pack_start(GTK_BOX(config_layout), enabled_layout, false, true, 0);
        gtk_box_pack_start(GTK_BOX(config_layout), make_layout, false, true, 0);
        gtk_box_pack_start(GTK_BOX(config_layout), model_layout, false, true, 0);
        gtk_box_pack_start(GTK_BOX(config_layout), pos_layout, false, true, 0);
        gtk_box_pack_start(GTK_BOX(config_layout), size_layout, false, true, 0);
        gtk_box_pack_start(GTK_BOX(config_layout), res_layout, false, true, 0);
        gtk_box_pack_start(GTK_BOX(config_layout), rot_layout, false, true, 0);

        GtkWidget *apply_button = gtk_button_new_with_label("Apply");
        GtkWidget *close_button = gtk_button_new_with_label("Close");

        GtkWidget *button_layout = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);

        gtk_box_pack_end(GTK_BOX(button_layout), close_button, false, false, 10);
        gtk_box_pack_end(GTK_BOX(button_layout), apply_button, false, false, 0);

        gtk_box_pack_start(GTK_BOX(interface_layout), drawing_area, true, true, 10);
        gtk_box_pack_start(GTK_BOX(interface_layout), config_layout, false, false, 10);

        gtk_box_pack_start(GTK_BOX(main_layout), main_label, false, true, 25);
        gtk_box_pack_start(GTK_BOX(main_layout), interface_layout, true, true, 0);
        gtk_box_pack_start(GTK_BOX(main_layout), button_layout, false, false, 10);

        gtk_container_add(GTK_CONTAINER(window), main_layout);

        g_signal_connect(drawing_area, "draw",
                         G_CALLBACK(draw), user_data);
        g_signal_connect(drawing_area, "button-press-event",
                         G_CALLBACK(draw_area_button_press_cb), user_data);
        gtk_widget_add_events(drawing_area, GDK_BUTTON_PRESS_MASK);

        g_signal_connect(outputs_cb, "changed",
                         G_CALLBACK(output_changed_cb), user_data);


        g_signal_connect(apply_button, "button-press-event",
                         G_CALLBACK(apply_button_press_cb), user_data);
        g_signal_connect(close_button, "button-release-event",
                         G_CALLBACK(close_button_release_cb), user_data);

        win->config = OutputConfig();
        win->config.outputs_cb = outputs_cb;
        win->config.make_data = make_data;
        win->config.model_data = model_data;
        win->config.pos_data = pos_data;
        win->config.size_data = size_data;
        win->config.res_cb = res_cb;
        win->config.rot_cb = rot_cb;

        gtk_window_set_title(GTK_WINDOW(window), "Displays");
        gtk_widget_show_all(window);
}

int
main(int argc, char **argv)
{
        GtkApplication *app;
        int status;
        Window *window;

        window = new Window();

        app = gtk_application_new("org.gtk.wayfire-config-manager", G_APPLICATION_FLAGS_NONE);
        g_signal_connect(app, "activate", G_CALLBACK(activate), window);
        status = g_application_run(G_APPLICATION(app), argc, argv);
        g_object_unref(app);

        delete window;

        return status;
}
